from typing import Any
from app.services.file_input import file_to_db
from app.utils.metadata import save_metadata
from app.api.crud.data import get_dataset, get_datasets
from fastapi import File, UploadFile, APIRouter, Form
from app.models.dataset import DatasetList, DatasetResponse
from fastapi import Query
from app.schemas.responses import DataStore200
from app.services.anonymize import anonymize_file_supression, arx_anonymizer

router = APIRouter()


@router.get("/datasets", response_model=DatasetList)
def get_list_of_dataset_names() -> Any:
    datasets = get_datasets()
    return {'datasets': datasets}


@router.get("/{dataset}", response_model=DatasetResponse)
def retrieve_anonymized_dataset(
        dataset: str = Query(None, description='Dataset name')
) -> Any:
    return get_dataset(dataset)


@router.post("/add", response_model=DataStore200)
async def import_dataset(
        metadata: str = Form(..., description='Form containing the same structure than the Metadata model'),
        file: UploadFile = File(..., description='.CSV file containing all the data. '
                                                 'The filename will be the dataset name.'),
) -> Any:
    # Store the metadata into the DB's "metadata" table
    _metadata = await save_metadata(metadata)
    # Anonymize the file
    await arx_anonymizer(file, metadata)
    # Store the file's data into the DB, in a table with the same name of the "dataset" field
    await file_to_db(_metadata)

    return DataStore200()
