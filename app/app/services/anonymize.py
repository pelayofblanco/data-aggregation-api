import time
import aiofiles
import pandas as pd
import json
import requests
import numpy as np


# Replace sensitive data fields with a '*'
async def anonymize_file_supression(file, metadata):
    # Write the file locally
    async with aiofiles.open('temp.csv', 'wb+') as out_file:
        while content := file.file.read(1024):  # async read chunk
            await out_file.write(content)  # async write chunk

    # Anonymize file
    df = pd.read_csv('temp.csv')
    metadata = json.loads(metadata)

    for header in metadata['sensitive_columns']:
        column_name = header[0]
        column_type = header[1]
        # Replace sensitive fields for '*'
        df[column_name] = df.loc[column_name] = '*'
    # Write to csv the anonymized data
    df.to_csv('temp.csv', sep=',', encoding='utf-8')


# Usage of arx-anonymizer container
async def arx_anonymizer(file, metadata):
    # Write the file locally
    async with aiofiles.open('temp.csv', 'wb+') as out_file:
        while content := file.file.read(1024):  # async read chunk.
            await out_file.write(content)  # async write chunk

    # Get metadata and sensitive columns
    metadata = json.loads(metadata)
    sensitive_columns = []
    for header in metadata['sensitive_columns']:
        sensitive_columns.append(header[0])

    # Read the file
    data = []
    sensitive_columns_indexes = []
    with open('temp.csv') as f:
        headers = f.readline().rstrip().split(',')
        # Get sensitive column indexes
        for header in sensitive_columns:
            sensitive_columns_indexes.append(headers.index(header))
        data.append(headers)
        # Get structured data from the csv
        for line in f:
            row = line.rstrip().split(',')
            data.append(row)
        f.close()

    # Get quasidentifying columns to generate hierarchies
    quasidentifying_data = [[]]
    hierarchies = []
    i = 0
    for index in sensitive_columns_indexes:
        for item in data[1:]:
            # Get hierarchies
            quasidentifying_data[i].append(item[index])
        i += 1
    # Send POST to the ARX hierarchy controller and append it
    hierarchies.append(generate_hierarchy(quasidentifying_data[0]))
    # Generate anonymize POST request
    arx_request = create_anonymize_request(data, headers, sensitive_columns, hierarchies=hierarchies)
    # Send the request
    anonymized_data = send_anonymize_request(arx_request)

    # Write anonymized data to the csv
    with open('temp.csv', 'w') as file:
        for line in anonymized_data:
            joined_line = ','.join(line)
            file.write(joined_line)
            file.write('\n')
        file.close()


def create_anonymize_request(data, headers, sensitive_columns, hierarchies):
    attributes = []
    i = 0
    for header in headers:
        if header not in sensitive_columns:
            attributes.append(
                {
                    "field": header,
                    "attributeTypeModel": "INSENSITIVE",
                    "hierarchy": None
                }
            )
        else:
            attributes.append(
                {
                    "field": header,
                    "attributeTypeModel": "QUASIIDENTIFYING",
                    "hierarchy": hierarchies[i]
                }
            )
            i += 1

    request = {
        "data": data,
        "attributes": attributes,
        "privacyModels": [
            {
                "privacyModel": "KANONYMITY",
                "params": {
                    "k": "5"
                }
            }
        ],
        "suppressionLimit": 0.02
    }
    return request


def send_anonymize_request(request):
    # print('REQUEST', request)
    headers = {'type': 'application/json'}
    response = requests.post("http://arx-anonymizer:8080/api/anonymize", json=request, headers=headers)
    # print(response.json())
    return response.json()['anonymizeResult']['data']


def generate_hierarchy(hierarchy):
    request = {
        "column": hierarchy,
        "builder": {
            "type": "redactionBased",
            "paddingCharacter": " ",
            "redactionCharacter": "*",
            "paddingOrder": "RIGHT_TO_LEFT",
            "redactionOrder": "RIGHT_TO_LEFT"
        }
    }
    headers = {'type': 'application/json'}
    response = requests.post("http://arx-anonymizer:8080/api/hierarchy", json=request, headers=headers)
    return response.json()['hierarchy']
