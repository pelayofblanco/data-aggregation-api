from ..utils import db
import pandas
import os
import csv


async def file_to_db(metadata):
    await csv_to_db(metadata)


async def csv_to_db(metadata):
    df = pandas.read_csv('temp.csv', quoting=csv.QUOTE_ALL)
    engine = db.get_engine()
    df.to_sql(con=engine, index=True, index_label='dapi_id', name=metadata.dataset, if_exists='replace')
    os.remove('temp.csv')
