from pydantic import BaseModel
from typing import List, Any
from app.schemas.metadata import Metadata


class DatasetList(BaseModel):
    datasets: List[str]


class DatasetResponse(BaseModel):
    data: List[Any]
    metadata: Metadata

    class Config:
        arbitrary_types_allowed = True
