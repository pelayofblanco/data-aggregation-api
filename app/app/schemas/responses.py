from pydantic import BaseModel


class DataStore200(BaseModel):
    status: str = 'OK'
    code: int = 200
    message: str = 'Data stored and anonymized successfully'
    error: bool = False

    class Config:
        orm_mode = True
